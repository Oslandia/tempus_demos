# Web demos

## How to use

Install dependencies: `npm install`

Copy conf and edit it to suit your need.
```bash
cp js/settings.js.example js/settings.js
```

Run dev server: `npm start`

# coding: utf-8

"""Luigi tasks downloading XML files and shapefiles from the Data Grand Lyon
Website https://data.grandlyon.com

You can get the names of available data after downloading the two "capabilities"
XML files from the WFS service.
"""

import os
import json
from collections import OrderedDict
from datetime import datetime as dt

from lxml import etree

import pandas as pd

import requests

import luigi
import luigi.postgres
from luigi.format import UTF8, MixedUnicodeBytes

_HERE = os.path.abspath(os.path.dirname(__file__))
WFS_RDATA_URL = "https://download.data.grandlyon.com/wfs/rdata"
WFS_GRANDLYON_URL = "https://download.data.grandlyon.com/wfs/grandlyon"
DEFAULT_PARAMS = {'SERVICE': 'WFS',
                  'VERSION': '2.0.0',
                  'request': 'GetFeature'}
DATADIR = 'datarepo'


def read_config(fname='config.ini'):
    """Read the configuration file and return the key/values in a dict
    """
    conf = {}
    with open(os.path.join(_HERE, fname)) as fobj:
        for line in fobj:
            key, value = line.split('=')
            conf[key.strip()] = value.strip()
    return conf

def layers(tree):
    """Get layer names from the XML Capabilities elements
    """
    ns = '{http://www.opengis.net/wms}'
    elements = tree.find(ns+'Capability').find(ns+'Layer').findall(ns+'Layer')
    g = ((x.get('queryable'), x.find(ns+'Name').text) for x in elements)
    return sorted(name for q,name in g if int(q))


def params_factory(projection, output_format, dataname):
    """return a new dict for HTTP query params

    Used for the wfs http query to get some data.
    """
    res = {"SRSNAME": 'EPSG:' + projection,
           "outputFormat": output_format,
           "typename": dataname}
    res.update(DEFAULT_PARAMS)
    return res


def get_all_typenames(source):
    """Get all layer names (i.e. typename) from a specific wfs source: rdata or
    grandlyon.
    """
    fname = os.path.join(DATADIR, '{}-layers.txt'.format(source))
    if source == 'rdata':
        url = WFS_RDATA_URL
    elif source == 'grandlyon':
        url = WFS_GRANDLYON_URL
    else:
        raise Exception("source {} not supported".format(source))
    url = 'https://download.data.grandlyon.com/wms/{}?SERVICE=WMS&REQUEST=GetCapabilities'.format(source)
    if os.path.isfile(fname):
        return open(fname).read().split()
    return layers(etree.fromstring(requests.get(url).content))


def layers_description(tree):
    """Get the name of the layer (aka typename) and its short description
    """
    result = []
    ns = '{http://www.opengis.net/wms}'
    elements = tree.find(ns+'Capability').find(ns+'Layer').findall(ns+'Layer')
    for element in elements:
        typename = element.find(ns+'Name').text
        description = element.find(ns+'Title').text
        result.append((typename, description))
    result.sort()
    return OrderedDict(result)


class ServiceCapabilitiesTask(luigi.Task):
    """Task to download XML files with some metadata
    """
    source = luigi.Parameter() # can accept rdata and grandlyon
    path = os.path.join(DATADIR, '{source}-capabilities.xml')

    def output(self):
        return luigi.LocalTarget(self.path.format(source=self.source), format=UTF8)

    def run(self):
        url = 'https://download.data.grandlyon.com/wms/{}?SERVICE=WMS&REQUEST=GetCapabilities'.format(self.source)
        resp = requests.get(url)
        with self.output().open("w") as fobj:
            fobj.write(resp.content.decode('utf-8'))

class LayersDescriptionTask(luigi.Task):
    """Get the name of each layer and its description
    """
    source = luigi.Parameter()
    path = os.path.join(DATADIR, '{source}-description.json')

    def output(self):
        return luigi.LocalTarget(self.path.format(source=self.source))

    def requires(self):
        return ServiceCapabilitiesTask(self.source)

    def run(self):
        with self.input().open() as fobj:
            tree = etree.parse(fobj)
        with self.output().open('w') as fobj:
            data = layers_description(tree)
            json.dump(data, fobj, indent=2, ensure_ascii=False)

class ExtractLayersTask(luigi.Task):
    """Task to extract layer names (i.e. typename) from a XML file.
    """
    source = luigi.Parameter()
    path = os.path.join(DATADIR, "{source}-layers.txt")

    def output(self):
        return luigi.LocalTarget(self.path.format(source=self.source), format=UTF8)

    def requires(self):
        return ServiceCapabilitiesTask(self.source)

    def run(self):
        with self.input().open() as fobj:
            tree = etree.parse(fobj)
        with self.output().open('w') as fobj:
            fobj.write("\n".join(layers(tree)))


class ExtractAllLayers(luigi.Task):
    """Extract all layer names for the rdata and grandlyon wfs data sources
    """
    def requires(self):
        yield ExtractLayersTask('rdata')
        yield ExtractLayersTask('grandlyon')

class ExtractAllJSONDescription(luigi.Task):
    """Extract all layer names and its description in a two JSON files.
    """
    def requires(self):
        yield LayersDescriptionTask('rdata')
        yield LayersDescriptionTask('grandlyon')

class ShapefilesTask(luigi.Task):
    """Task to download a zip files which includes the shapefile

    Need the source: rdata or grandlyon and the layer name (i.e. typename).
    """
    source = luigi.Parameter()
    typename = luigi.Parameter()
    path = os.path.join(DATADIR , '{typename}.zip')
    srid = 4326

    def requires(self):
        return ExtractLayersTask(self.source)

    def output(self):
        return luigi.LocalTarget(self.path.format(typename=self.typename),
                                 format=MixedUnicodeBytes)

    def run(self):
        if self.source == 'rdata':
            url = WFS_RDATA_URL
        elif self.source == 'grandlyon':
            url = WFS_GRANDLYON_URL
        else:
            raise Exception("source {} not supported".format(self.source))
        params = params_factory(str(self.srid), 'SHAPEZIP', self.typename)
        with self.output().open('w') as fobj:
            resp = requests.get(url, params=params)
            resp.raise_for_status()
            fobj.write(resp.content)


class WrapperShapeTask(luigi.Task):
    """A wrapper to download all zip files (including the shapefile) for several
    layers and a specific source: rdata or grandlyon.
    """
    source = luigi.Parameter()

    def requires(self):
        yield ExtractLayersTask(self.source)
        for typename in get_all_typenames(self.source):
            yield ShapefilesTask(self.source, typename)

class VelovStationAvailability(luigi.Task):
    """Get in real-time the shared cycle stations avaibility in a JSON format.

    Get data every 5 minutes
    """
    timestamp = luigi.DateMinuteParameter(default=dt.now(), interval=5)
    path = os.path.join(DATADIR, 'velov', '{year}', '{month:02d}', '{day:02d}', '{ts}.json')

    def output(self):
        triple = lambda x: (x.year, x.month, x.day)
        year, month, day = triple(self.timestamp)
        ts = self.timestamp.strftime("%HH%M") # 16H35
        return luigi.LocalTarget(self.path.format(year=year, month=month, day=day, ts=ts), format=UTF8)

    def run(self):
        url = 'https://download.data.grandlyon.com/ws/rdata/jcd_jcdecaux.jcdvelov/all.json'
        with self.output().open('w') as fobj:
            resp = requests.get(url)
            resp.raise_for_status
            data = resp.json()
            json.dump(resp.json(), fobj, ensure_ascii=False)


class VelovStationJSONtoCSV(luigi.Task):
    """Turn real-time velov station data JSON file to a CSV.
    """
    timestamp = luigi.DateMinuteParameter(default=dt.now(), interval=5)
    path = os.path.join(DATADIR, 'velov', '{year}', '{month:02d}', '{day:02d}', '{ts}.csv')
    keepcols = ['number', 'last_update', 'bike_stands', 'available_bike_stands',
                'available_bikes', 'availabilitycode', 'availability', 'bonus',
                'status']

    def output(self):
        triple = lambda x: (x.year, x.month, x.day)
        year, month, day = triple(self.timestamp)
        ts = self.timestamp.strftime("%HH%M") # 16H35
        return luigi.LocalTarget(self.path.format(year=year, month=month, day=day, ts=ts), format=UTF8)

    def requires(self):
        return VelovStationAvailability(self.timestamp)

    def run(self):
        with self.input().open() as fobj:
            data = json.load(fobj)
            df = pd.DataFrame(data['values'], columns=data['fields'])
        with self.output().open('w') as fobj:
            df[self.keepcols].to_csv(fobj, index=False)

class VelovStationDatabase(luigi.postgres.CopyToTable):
    """Insert Velov stations data into a PostgreSQL table
    """
    timestamp = luigi.DateMinuteParameter(default=dt.now(), interval=5)

    host = 'localhost'
    database = read_config()['dbname']
    # user = 'tempus'
    user = read_config()['dbuser']
    password = None
    table = '{schema}.velov_timeserie'.format(schema=read_config()['schema'])

    columns = [('number', 'INT'),
               ('last_update', 'TIMESTAMP'),
               ('bike_stands', 'INT'),
               ('available_bike_stands', 'INT'),
               ('available_bikes', 'INT'),
               ('availabilitycode', 'INT'),
               ('availability', 'VARCHAR(20)'),
               ('bonus', 'VARCHAR(12)'),
               ('status', 'VARCHAR(12)')]

    def rows(self):
        """overload the rows method to skip the first line (header)
        """
        with self.input().open('r') as fobj:
            df = pd.read_csv(fobj)
            for idx, row in df.iterrows():
                yield row.values

    def requires(self):
        return VelovStationJSONtoCSV(self.timestamp)

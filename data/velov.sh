#!/bin/bash

here="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export PYTHONPATH=$here

workers=2
# luigi --local-scheduler --workers $workers --parallel-scheduling --module tasks VelovStationAvailability
luigi --local-scheduler --workers $workers --parallel-scheduling --module tasks VelovStationJSONtoCSV

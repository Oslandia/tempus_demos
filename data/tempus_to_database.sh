#!/bin/bash

set -u
set -e

echo "-----   INFO  ------"
echo "Insert roads and public transport data into a PostgreSQL database for tempus"
echo "You need postgis, pgrouting, hstore, plpgsql and plpythonu extensions"
echo ""

if [ ! -f "config.ini" ]; then
    echo "ERROR: you need a 'config.ini' file"
    exit 0
fi

# load config.ini
dbname=$(grep -Po "(?<=dbname=).*" config.ini)
dbuser=$(grep -Po "(?<=dbuser=).*" config.ini)
# schema=$(grep -Po "(?<=schema=).*" config.ini)
pgport=$(grep -Po "(?<=pgport=).*" config.ini)
srid=$(grep -Po "(?<=srid=).*" config.ini)

echo "#### Configuration variables"
echo "dbname: $dbname"
echo "dbuser: $dbuser"
# echo "schema: $schema"
echo "pgport: $pgport"
echo "srid: $srid"
echo ""


# OSM Roads
loadtempus -t osm -d "dbname=$dbname user=postgres" -R -s lyon.osm.pbf

# GTFS public transport
loadtempus -S $srid -t gtfs --pt-create-roads --pt-network tcl -d "dbname=$dbname user=postgres"  -s GTFS_TCL.ZIP

# Shared cycle stations
loadtempus -S $srid -t poi --poi-service-name "station_velov" --poi-name-field "nom" -y 4 -d "dbname=$dbname user=postgres"  -s data/shapefiles/pvo_patrimoine_voirie.pvostationvelov.shp

# Shared car stations
loadtempus -S $srid -t poi --poi-service-name "station_car" --poi-name-field "nom" -y 2 -d "dbname=$dbname user=postgres"  -s data/shapefiles/pvo_patrimoine_voirie.pvostationautopartage.shp

# Parking
loadtempus -S $srid -t poi --poi-service-name "parking" --poi-name-field "nom" -y 1 -d "dbname=$dbname user=postgres"  -s data/shapefiles/pvo_patrimoine_voirie.pvoparking.shp

Ansible playbook to install Tempus and a PostGIS db on an Ubuntu 16.04. 

**Require ansible >= 2.2**


# How to use

Create a playbook with the role you want. Take inspiration from:
-`vars` file in `vars/`
-`playbok.yml` and `playbook-prod.yml`
-`hosts` file

For example, to deploy a complete demo based on apache on a server named olympus in your ssh config, you can execute:

```bash
ansible-playbook playbook-prod.yml -i hosts -vv
```

# Roles

## common

Install some common package and create users.

## database

Create the database environnement, the user and the database specified in vars file.

## tempus

Initiate code env:
- compile tempus_core
- install needed python modules in virtualenv
- configure uwsgi for tempus_geojson_server

## tempus-web

Prepare and pack the web UI.

## tempus-apache

Apache configuration for this demo.
